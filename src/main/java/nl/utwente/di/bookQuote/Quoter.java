package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    HashMap<String, Double> bookMap;

    public Quoter() {
        bookMap = new HashMap<>();
        bookMap.put("1", 10.0);
        bookMap.put("2", 45.0);
        bookMap.put("3", 20.0);
        bookMap.put("4", 35.0);
        bookMap.put("5", 50.0);
    }

    public double getBookPrice(String isbn) {
        if (bookMap.containsKey(isbn)) {
            return bookMap.get(isbn);
        }
        return 0;
    }
}
